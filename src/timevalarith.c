/* $Id$ */

/***
  This file is part of ivam2.

  ivam2 is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  ivam2 is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the GNU General Public License
  along with ivam2; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "timevalarith.h"

struct timeval timeval_max(struct timeval a, struct timeval b) {

    if (a.tv_sec > b.tv_sec)
        return a;

    if (a.tv_sec < b.tv_sec)
        return b;

    if (a.tv_usec > b.tv_usec)
        return a;

    return b;
}

struct timeval timeval_add(struct timeval a, uint32_t d) {
    uint64_t sec, usec;
    struct timeval r;

    sec = (uint64_t) a.tv_sec + (d / 1000000L);
    usec = (uint64_t) a.tv_usec + (d % 1000000L);

    while (usec > 1000000L) {
        usec -= 1000000L;
        sec++;
    }

    r.tv_sec = (long) sec;
    r.tv_usec = (long) usec;
    return r;
}

struct timeval timeval_sub(struct timeval a, uint32_t d) {
    uint64_t sec, usec;
    struct timeval r;

    sec = (uint64_t) a.tv_sec - (d / 1000000L);
    usec = (uint64_t) a.tv_usec;

    d = (d % 1000000L);
    
    if (usec > d)
        usec -= d;
    else {
        sec--;
        usec = 1000000L + usec - d;
    }

    r.tv_sec = (long) sec;
    r.tv_usec = (long) usec;
    return r;
}
