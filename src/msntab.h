#ifndef foomsntabhfoo
#define foomsntabhfoo

/* $Id$ */

/***
  This file is part of ivam2.

  ivam2 is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  ivam2 is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the GNU General Public License
  along with ivam2; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
***/

enum call_action { CALL_ACTION_IGNORE, CALL_ACTION_ACCEPT, CALL_ACTION_HANGUP };

struct tabentry {
    enum call_action action;
    unsigned ref_counter;

    char **args;
    char *local;
    char *remote;

    char *timespec;
    char *dayspec;
    
    struct tabentry *next;
    struct tabentry *prev;

    int shbuf;
    unsigned rings;
    int pipehack;

    char *filename; /* filename of the msntab where this entry originates from */
    unsigned line;
};

struct tabentry* msntab_check_call(const char *callee, const char *caller);
struct tabentry* msntab_ref(struct tabentry *t);
void msntab_unref(struct tabentry *t);

void msntab_flush(void);
int msntab_load(const char *fn);
void msntab_dump(void);

#endif
