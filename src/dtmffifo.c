/* $Id$ */

/***
  This file is part of ivam2.

  ivam2 is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  ivam2 is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the GNU General Public License
  along with ivam2; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <unistd.h>
#include <libdaemon/dlog.h>
#include <limits.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <sys/stat.h>

#include "dtmffifo.h"

struct dtmf_fifo* dtmf_fifo_new(void) {
    struct dtmf_fifo *d = NULL;
    char p[PATH_MAX];
    mode_t u;

    d = malloc(sizeof(struct dtmf_fifo));
    assert(d);
    memset(d, 0, sizeof(struct dtmf_fifo));
    d->fd = -1;

    d->dir = strdup("/tmp/ivamd.XXXXXX");
    assert(d->dir);

    u = umask(0077);
    if (!mkdtemp(d->dir)) {
        umask(u);
        daemon_log(LOG_ERR, "Failed to create temporary directory '%s': %s", d->dir, strerror(errno));
        goto fail;
    }

    snprintf(p, sizeof(p), "%s/%s", d->dir, "dtmf");
    d->fname = strdup(p);
    assert(d->fname);

    if (mkfifo(d->fname, 0700) != 0) {
        umask(u);
        daemon_log(LOG_ERR, "Failed to create FIFO '%s': %s", d->fname, strerror(errno));
        goto fail;
    }

    umask(u);
    
    if ((d->fd = open(d->fname, O_RDWR|O_NDELAY)) < 0) {
        daemon_log(LOG_ERR, "Failed to open FIFO '%s': %s", d->fname, strerror(errno));
        goto fail;
    }
        
    daemon_log(LOG_INFO, "Sucessfully opened DTMF FIFO '%s'.", d->fname);
    
    return d;

fail:
    if (d)
        dtmf_fifo_free(d);

    return NULL;
}

void dtmf_fifo_free(struct dtmf_fifo *d) {
    assert(d);

    if (d->fd >= 0)
        close(d->fd);

    if (d->fname) {
        unlink(d->fname);
        free(d->fname);
    }
    
    if (d->dir) {
        rmdir(d->dir);
        free(d->dir);
    }
    
    free(d);
}

void dtmf_fifo_pass(struct dtmf_fifo *d, char c) {
    assert(d && d->fd >= 0);
    
    //daemon_log(LOG_INFO, "Recieved DTMF character '%c'", c);

    if (write(d->fd, &c, 1) != 1)
        daemon_log(LOG_ERR, "Failed to write to DTMF FIFO: %s", strerror(errno));
}
