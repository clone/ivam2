#ifndef foomodemhfoo
#define foomodemhfoo

/* $Id$ */

/***
  This file is part of ivam2.

  ivam2 is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  ivam2 is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the GNU General Public License
  along with ivam2; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
***/

#include "buffio.h"
#include "msntab.h"

enum modem_state {
    MODEM_STATE_INIT,
    MODEM_STATE_CALLER_NUMBER_EXPECT,
    MODEM_STATE_CALLER_NUMBER,
    MODEM_STATE_RING_EXPECT,
    MODEM_STATE_RING,
    MODEM_STATE_RINGING,
    MODEM_STATE_ATA,
    MODEM_STATE_VTXVRX,
    MODEM_STATE_CONNECTION,
    MODEM_STATE_CONNECTION_SHBUF,
    MODEM_STATE_PRE_HANGUP_HUPSEQ,
    MODEM_STATE_PRE_HANGUP_ATHSEQ,
    MODEM_STATE_HANGUP,
    MODEM_STATE_CHILD_DRAIN,      /* wait until no further data may/has to be written to the child */
    MODEM_STATE_CHILD_WAIT_EOF,   /* wait until no further data may be read from child */
    MODEM_STATE_CHILD_KILL,
    MODEM_STATE_IDLE
};

#define MODEM_BUF_LEN (10*1024)

struct modem {
    char *dev;
    char *name;

    struct buffio *buffio;
    
    enum modem_state state;
    int command_index;

    struct buffio *child_buffio;
    pid_t child_pid;

    struct dtmf_fifo *dtmf_fifo;

    struct timeval timeout;

    char *ring_number;
    char *caller_number;

    char *listen_msn;

    struct tabentry *tabentry;

    int dle_flag;
    int flush_msg;
};

struct modem *modem_open(const char *dev, const char *msn);
void modem_close(struct modem *m);

#endif
