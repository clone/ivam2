#ifndef foodlehfoo
#define foodlehfoo

/* $Id$ */

/***
  This file is part of ivam2.

  ivam2 is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  ivam2 is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the GNU General Public License
  along with ivam2; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
***/

#include <sys/types.h>
#include <inttypes.h>

/* Those nifty DLE-sequences */
#define DLE 0x10
#define ETX 0x03
#define DC4 0x14

size_t dle_decode(const uint8_t* s, size_t ls, uint8_t* d, size_t *ld, int (*dle_func) (uint8_t c, void *user), void *user, int *dle_flag);
size_t dle_encode(const uint8_t* s, size_t ls, uint8_t* d, size_t *ld);

#endif
