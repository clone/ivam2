#ifndef foobuffiohfoo
#define foobuffiohfoo

/* $Id$ */

/***
  This file is part of ivam2.

  ivam2 is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  ivam2 is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the GNU General Public License
  along with ivam2; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
***/

#include <inttypes.h>
#include <sys/types.h>

enum buffio_sched_cb {
    BUFFIO_SCHED_CB_IDLE = 0,
    BUFFIO_SCHED_CB_INPUT_READY = 1,
    BUFFIO_SCHED_CB_OUTPUT_REQUEST = 2,
    BUFFIO_SCHED_CB_OUTPUT_EMPTY = 32,
    BUFFIO_SCHED_CB_EOF = 4,
    BUFFIO_SCHED_CB_EPIPE = 8,
    BUFFIO_SCHED_CB_ERROR = 16
};

struct buffio {
    int ifd;
    int ofd;

    uint8_t *input_buf;
    size_t input_max_length, input_index, input_length, input_range;

    uint8_t *output_buf;
    size_t output_max_length, output_index, output_length, output_range;

    int b_read_cb, b_write_cb;

    void *user;

    int readable, writable;
    int prebuf;

    int (*input_ready_cb) (struct buffio *b, void *user);
    int (*output_request_cb) (struct buffio *b, void *user);
    int (*output_empty_cb) (struct buffio *b, void *user);
    int (*eof_cb) (struct buffio *b, void *user);
    int (*epipe_cb) (struct buffio *b, void *user);
    int (*error_cb) (struct buffio *b, void *user);

    enum buffio_sched_cb sched_cb;
    
    uint32_t output_delay_usec;
    uint32_t output_latency_usec;
    int delaying;
    struct timeval finish_tv, timeout_tv;
};

struct buffio* buffio_new(int ifd, int ofd);
void buffio_free(struct buffio *b);

void buffio_write(struct buffio *b, const uint8_t *d, size_t l);
void buffio_print(struct buffio *b, const char *s);

void buffio_flush_input(struct buffio *b);
void buffio_flush_output(struct buffio *b);

void buffio_command(struct buffio *b, const char *c);
int buffio_find_input(struct buffio *b, const char *c);
char *buffio_read_line(struct buffio *b, char *c, size_t l);

void buffio_dump(struct buffio *b);

void buffio_set_input_range(struct buffio *b, ssize_t w);
void buffio_set_output_range(struct buffio *b, ssize_t w);

const uint8_t* buffio_read_ptr(struct buffio *b, size_t *l);
void buffio_read_ptr_inc(struct buffio *b, size_t l);

uint8_t* buffio_write_ptr(struct buffio *b, size_t *l);
void buffio_write_ptr_inc(struct buffio *b, size_t l);

void buffio_close_input_fd(struct buffio *b);
void buffio_close_output_fd(struct buffio *b);

int buffio_output_is_empty(struct buffio *b);
int buffio_input_is_full(struct buffio *b);

void buffio_dump_lines(struct buffio *b);

void buffio_set_fds(struct buffio *b, int ifd, int ofd);

void buffio_set_delay_usec(struct buffio *b, unsigned long usec, unsigned long latency);

int buffio_can_write(struct buffio *b, size_t l);

void buffio_set_prebuf(struct buffio *b, int p);

#endif
