/* $Id$ */

/***
  This file is part of ivam2.

  ivam2 is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  ivam2 is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the GNU General Public License
  along with ivam2; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
***/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <string.h>
#include <unistd.h>

#include "util.h"

char *basename(char *path) {
    char *p;

    if ((p = strrchr(path, '/')))
        return p+1;

    return path;
}

ssize_t loop_read(int FILEDES, void *BUFFER, size_t SIZE) {
    ssize_t c = 0;
    
    while (SIZE > 0) {
        ssize_t r = read(FILEDES, BUFFER, SIZE);

        if (r <= 0)
            break;

        SIZE -= r;
        c += r;
        BUFFER += r;
    }

    return c;
}

ssize_t loop_write(int FILEDES, const void *BUFFER, size_t SIZE) {
    ssize_t c = 0;
    
    while (SIZE > 0) {
        ssize_t r = write(FILEDES, BUFFER, SIZE);

        if (r <= 0)
            break;

        SIZE -= r;
        c += r;
        BUFFER += r;
    }

    return c;
}
