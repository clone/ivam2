#ifndef footimevalarithhfoo
#define footimevalarithhfoo

/* $Id$ */

/***
  This file is part of ivam2.

  ivam2 is free software; you can redistribute it and/or modify it
  under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
  
  ivam2 is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
  License for more details.
  
  You should have received a copy of the GNU General Public License
  along with ivam2; if not, write to the Free Software Foundation,
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
***/

#include <sys/types.h>
#include <inttypes.h>

struct timeval timeval_max(struct timeval a, struct timeval b);
struct timeval timeval_add(struct timeval a, uint32_t d);
struct timeval timeval_sub(struct timeval a, uint32_t s);

#endif
