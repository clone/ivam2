<?xml version="1.0" encoding="iso-8859-1"?> <!-- -*-html-helper-*- -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
<title>ivam2 @PACKAGE_VERSION@</title>
<link rel="stylesheet" type="text/css" href="style.css" />
</head>

<body>
<h1><a name="top">ivam2 @PACKAGE_VERSION@</a></h1>

<p><i>Copyright 2004 Lennart Poettering &lt;@PACKAGE_BUGREPORT@&gt;</i></p>

<ul class="toc">
    <li><a href="#license">License</a></li>
    <li><a href="#news">News</a></li>
    <li><a href="#overview">Overview</a></li>
    <li><a href="#status">Status</a></li>
    <li><a href="#documentation">Documentation</a></li>
    <li><a href="#requirements">Requirements</a></li>
    <li><a href="#installation">Installation</a></li>
    <li><a href="#acks">Acknowledgements</a></li>
    <li><a href="#download">Download</a></li>
</ul>

<h2><a name="license">License</a></h2>

<p>This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the
License, or (at your option) any later version.</p>

<p>This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.</p>

<p>You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.</p>

<h2><a name="news">News</a></h2>

<div class="news-date">Mon Feb 9 2004: </div>

<p class="news-text"><a href="@PACKAGE_URL@ivam2-0.3.tar.gz">Version
0.3</a> released, changes include: time dependent configuration; this
version breaks <tt>msntab</tt> compatibility as two new columns (time
and date) are introduced - <b>you need update the file manually</b>; compatibility fixes with Python 2.2; assorted bug fixes</p>

<div class="news-date">Mon Jan 26 2004: </div>

<p class="news-text"><a href="@PACKAGE_URL@ivam2-0.2.tar.gz">Version 0.2</a> released, changes include: compatiblity with non-C99 compilers (such as gcc 2.95), python script fixes.</p>

<div class="news-date">Mon Jan 19 2004: </div>

<p class="news-text"><a href="@PACKAGE_URL@ivam2-0.1.tar.gz">Version 0.1</a> released, the initial release</p>

<h2><a name="overview">Overview</a></h2>

<p><tt>ivam2</tt> is an automatic phone answering machine software for ISDN
and Linux. It is the completely rewritten successor of <a
href="http://ivam.sf.net/">ivam</a> featuring many additions.</p>

<p><tt>ivam2</tt> is seperated in two parts: the core daemon written
in C and the automate logic coded in <a
href="http://www.python.org/">Python</a>. The latter is pluggable and
may be replaced by different implementations on a per phone number
basis (both caller and callee). This makes <tt>ivam2</tt> a very
powerful application server for telephony services. The software is
very scalable, multiple ISDN channels may be controlled from a single
daemon. To write telephony applications for <tt>ivam2</tt> is not
complicated. In fact, they are simple executables which read audio
data of the caller from STDIN and write audio data for the caller to
STDOUT. DTMF sequences may be read from a FIFO special file. A
framework for writing telephony applications in Python is provided, a
simple answering machine script based on this framework as well. You
are free to write applications in other languages such as Perl or
C.</p>

<h2><a name="status">Status</a></h2>

<p>Version @PACKAGE_VERSION@ is more or less stable and feature complete.</p>
  
<h2><a name="documentation">Documentation</a></h2>

<p>Have a look on the man pages <a
href="@PACKAGE_URL@ivamd.8.xml"><tt>ivamd(8)</tt></a>, <a
href="@PACKAGE_URL@ivam2.conf.5.xml"><tt>ivam2.conf(5)</tt></a>, <a
href="@PACKAGE_URL@msntab.5.xml"><tt>msntab(5)</tt></a>, <a
href="@PACKAGE_URL@ivam-autobox.1.xml"><tt>ivam-autobox(1)</tt></a>,
<a
href="@PACKAGE_URL@ivam-voicebox.1.xml"><tt>ivam-voicebox(1)</tt></a>,
<a href="@PACKAGE_URL@ivam-echo.1.xml"><tt>ivam-echo(1)</tt></a>, <a
href="@PACKAGE_URL@ivam-play.1.xml"><tt>ivam-play(1)</tt></a>, <a
href="@PACKAGE_URL@ivam-createvb.1.xml"><tt>ivam-createvb(1)</tt></a>,
<a
href="@PACKAGE_URL@ivam-spool.5.xml"><tt>ivam-spool(5)</tt></a>. (An
XSLT capable browser is required)</p>

<p>The core daemon <tt>ivamd</tt> is configured through both the
command line arguments and the file <tt>msntab</tt>. The latter
specifies which telephony application to start for which caller/callee
pairs. An example:</p>

<pre># Local  Remote   Days Times       Options          Action
47110816 *        *    *           defaults         @ignore
47110815 *        *    12:00-12:15 defaults         @ignore
47110815 12345678 *    *           defaults         @hangup
47110815 87654321 *    *           rings=2          ivam-play /path/to/an/ulaw/file.ulaw.gz
47110815 47110815 *    *           rings=0,pipehack ivam-autobox
47110815 *        *    *           rings=8,pipehack ivam-autobox
</pre>

<p>This configuration can be explained as follows: all calls to the
local phone number 47110816 are ignored. Calls to the local phone
number 47110815 are dispatched as follows: during lunch break all calls
are ignored, if if the caller is 87654321, he will listen to a static
message after two rings. If someone is calling the local number from
the same number he enters the voice box system immediately. All other
callers will be dispatched to the voice box system after eight
rings.</p>

<p>For more information on this configuration file see the manual page <a href="@PACKAGE_URL@msntab.5.xml"><tt>msntab(5)</tt></a>.</p>

<p>The one and only audio format <tt>ivam2</tt> uses is 8 bit uLaw at
8000Hz. Audio files stored on harddisk are raw sample data without any
header information. You are free to compress the audio files with
<tt>gzip</tt> to save some bytes. You can create these files by
recording them with a telephone and <a
href="http://0pointer.de/lennart/projects/ivcall/"><tt>ivcall</tt></a>.</p>

<p>Another way to create these files is by using <tt>sox</tt> to convert standard <tt>.wav</tt> files:</p>

<pre>sox foo.wav -t raw -U -b -r 8000 welcome.ulaw</pre>

<p>The distribution of <tt>ivam2</tt> comes with four telephony applications:</p>

<ul>
  <li><tt>ivam-echo</tt> is a simple (shell) script which writes all incoming
  audio data back to the caller. This may be used for testing or latency
  measuring purposes. Internally this is not much more than a simple
  call to <tt>/bin/cat</tt>.</li>

  <li><tt>ivam-play</tt> is a simple (shell) script which plays the
  contents of a <tt>.ulaw(.gz)</tt> file to the caller.</li>

  <li><tt>ivam-voicebox</tt> is a voice box application based on the
  Python framework described above. Audio files are saved in a special
  spool directory, by default
  <tt>/var/spool/ivam2/[VOICEBOXNAME]</tt>. Many voice boxes may
  coexist on the same system and configured individually. The voice
  box supports recording of messages for unauthenticated users and
  playback and deletion of messages for authenticates
  users. Authentication is based on an arbitrary length PIN. When a
  new voice message arrives a mail (including an Ogg Vorbis encoded version
  of the voice messages) is sent to the owner of the voice
  box.</li>

  <li><tt>ivam-autobox</tt> is a more capable version of
  <tt>ivam-voicebox</tt>. Internally both applications use the same
  logic. <tt>ivam-autobox</tt> is able to create spool directories
  <i>on-the-fly</i>. If a phone call to a local phone number has no
  existent voice box assigned one is automatically created and the
  administrator is notified.</li>
  
</ul>

<p>Have a look on the respective manual pages for more information on
these applications.</p>

<p>The default audio samples for <tt>ivam-voicebox</tt> and
<tt>ivam-autobox</tt> are in german language as this is my native
tongue. You probably want to replace them with messages in your own
language. (<i>I am interested in adding high quality english language
samples to the distribution. If you are a native speaker and think
that you are capable of creating them, feel free to contact me.</i>)
Have a look on the manual page <a
href="@PACKAGE_URL@ivam-spool.5.xml"><tt>ivam-spool(5)</tt></a> for
more information how to change the audio samples.</p>

<h2><a name="requirements">Requirements</a></h2>

<p>A current Linux kernel with <a
href="http://www.isdn4linux.de/">isdn4linux</a> and a compatible and
configured (active or passive) ISDN card is required.</p>

<p><tt>ivam2</tt> relies on a current version of <a
href="http://www.python.org/">Python</a>. It was developed on Python
2.3, but there are user reports that it works on Python 2.2 without
changes.</p>

<p><tt>ivam2</tt> needs <a
href="http://0pointer.de/lennart/projects/libdaemon/"><tt>libdaemon</tt></a> (>= 0.4)
and <a href="http://www.liboop.org/"><tt>liboop</tt></a> (>= 1.0).</p>

<p><b>Please note: the current <tt>liboop-dev</tt> package in Debian
unstable (1.0-1) is broken as the <tt>pkg-config</tt> file contained
in the upstream distribution is missing. <tt>ivam2</tt>'s
<tt>configure</tt> script requires that file. Thus you have to compile
your own version of <tt>liboop</tt> in order to use
<tt>ivam2</tt>. Please beg the Debian maintainer of <tt>liboop</tt> so
that he fixes this. For more information see Debian bug report <a href="http://bugs.debian.org/227061">#227061</a>.</b></p>

<p>If you plan to use the message notification feature of
<tt>ivam2</tt> you need <tt>metamail</tt>, <tt>sox</tt> and <tt>oggenc</tt>
installed on the local system.</p>

<h2><a name="installation">Installation</a></h2>

<p>As this package is made with the GNU <tt>autotools</tt> you should
run <tt>./configure</tt> inside the distribution directory for
configuring the source tree. After that you should run <tt>make</tt>
for compilation and <tt>make install</tt> (as root) for installation
of <tt>ivam2</tt>. A SysV compatible init script is installed to your
init directory.</p>

<p>After installation of the executables you need to create an user
<tt>ivam</tt> and a matching group <tt>ivam</tt>. The spool directory
(by default <tt>/var/spool/ivam2</tt>) has to be created and must be
owned by <tt>ivam:ivam</tt>.</p>

<p>You should edit the configuration file <tt>msntab</tt> to enable
the telephony services you want to use.</p>


<h2><a name="acks">Acknowledgements</a></h2>

<p>Wolfram Schlich for many suggestions and beta testing.</p>

<p>Wolfgang Ocker for some patches.</p>

<h2><a name="download">Download</a></h2>

<p>The newest release is always available from <a href="@PACKAGE_URL@">@PACKAGE_URL@</a></p>

<p>The current release is <a href="@PACKAGE_URL@ivam2-@PACKAGE_VERSION@.tar.gz">@PACKAGE_VERSION@</a></p>

<p>Get <tt>ivam2</tt>'s development sources from the <a href="http://subversion.tigris.org/">Subversion</a> <a href="https://seth.intheinter.net:8081/svn/ivam2/">repository</a>.</p>

<p>If you want to be notified whenever I release a new version of this software use the subscription feature of <a href="http://freshmeat.net/projects/ivam/">Freshmeat</a>.</p>

<hr/>
<address class="grey">Lennart Poettering &lt;@PACKAGE_BUGREPORT@&gt;, February 2004</address>
<div class="grey"><i>$Id$</i></div>

</body>
</html>
