# $Id$
#
# This file is part of ivam2.
#
# ivam2 is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# ivam2 is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ivam2; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.

from ivamApi import ivamConnector

class ivamShbufConnector(ivamAnsweringMachine):

    def run(self):
        pass

    def playClip(self):
        pass

    def stopPlayback(self):
        pass

    def softStopPlayback(self):
        pass

    def recordClip(self):
        pass

    def stopRecording(self):
        pass

    def timeout(self):
        pass

