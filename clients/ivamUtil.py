# $Id$
#
# This file is part of ivam2.
#
# ivam2 is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# ivam2 is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ivam2; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.

import gzip

def getContents(fn):
    f = file(fn, "r")
    r = f.readline().strip(" \t\r\n")
    f.close()
    return r


def setContents(fn, s):
    f = file(fn, "w")
    f.write("%s\n" % s)
    f.close()

def magicFile(fn, mode):

    f = None
    
    try:
        f = gzip.open(fn, mode)
        f.read(1)
        f.seek(0)
        return f
        
    except IOError:

        if not f is None:
            f.close()

    return open(fn, mode)
            
        

    

