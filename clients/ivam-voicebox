#!/usr/bin/python

# $Id$
#
# This file is part of ivam2.
#
# ivam2 is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# ivam2 is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ivam2; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.

import sys, os, getopt

from ivamCore import log
from ivamUtil import getContents, setContents
import ivamCore, ivamVoiceBox

def usage():
    log("%s [--record-time=SECS] [--welcome-message=MESSAGE] [--pin=PIN] [--pin-file=PINFILE] [--message-program=BINARY] [--debug] [--record-only] [--no-record] DIRECTORY" % sys.argv[0])

def parseArgs(vb, argv):

    try:
        opts, args = getopt.getopt(argv[1:], "hd", ["help", "record-time=", "welcome-message=", "pin=", "pin-file=", "debug", "message-program=", "record-only", "no-record"])
    except getopt.GetoptError:
        usage()
        sys.exit(1)

    try:
        dname = args[0]
    except IndexError:
        usage()
        sys.exit(1)
        
    vb.setDirectory(dname)

    for o, a in opts:
        if o in ("-d", "--debug",):
            ivamCore.DEBUG = True
            
        elif o in ("-h", "--help",):
            usage()
            sys.exit()
            
        elif o in ("--record-time",):
            vb.recordTime = int(a)
            
        elif o in ("--welcome-message",):
            vb.setWelcomeMessage(a)
 
        elif o in ("--pin",):
            vb.setPin(a)
            
        elif o in ("--pin-file",):
            vb.setPin(getContents(a))
            
        elif o in ("--message-program",):
            vb.messageProgram = a

        elif o in ("--record-only",):
            vb.recordOnly = True

        elif o in ("--no-record",):
            vb.noRecord = True


def main():
    vb = ivamVoiceBox.VoiceBox()
    parseArgs(vb, sys.argv)
    ivamCore.newConnector(vb).run()
    sys.exit()

if __name__ == "__main__":
    main()
