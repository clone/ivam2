# $Id$
#
# This file is part of ivam2.
#
# ivam2 is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# ivam2 is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ivam2; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.

class Processor:

    def onConnect(self, c, callerNumber, ringNumber):
        pass

    def onHangup(self, c):
        pass

    def onDtmfEvent(self, c, event):
        pass

    def onClipFinish(self, c, fname, length):
        pass

    def onTimeout(self, c):
        pass

    def onRecordFinish(self, c, fname):
        pass

class Connector:

    def run(self):
        pass

    def playClip(self, fname):
        pass

    def stopPlayback(self):
        pass

    def stopPlayback2(self):  # Same as previous but call onClipFinish
        pass

    def recordClip(self, fname, gzip = False):
        pass

    def stopRecording(self):
        pass

    def setTimeout(self, t):
        pass

    def hangup(self, t):
        pass

    def flushOutput(self):
        pass
