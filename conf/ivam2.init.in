#!/bin/bash
# $Id$

# This file is part of ivam2.
#
# ivam2 is free software; you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free
# Software Foundation; either version 2 of the License, or (at your
# option) any later version.
#
# ivam2 is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License
# along with ivam2; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.

CFG="@pkgsysconfdir@/ivam2.conf"

IVAMD="@sbindir@/ivamd"
test -x "$IVAMD" || exit 0

if [ `id -u` != "0" ] && [ "$1" = "start" -o "$1" = "stop" ] ; then
  echo "You must be root to start, stop or restart ivamd."
  exit 1
fi

[ -f "$CFG" ] && . "$CFG"

case "$1" in
    start)
        echo -n "Starting ISDN Voice Box Answering Machine: "
        PYTHONPATH="@pythondir@" "$IVAMD" "$ARGS" && echo "ivamd"
        ;;

    stop)
        echo -n "Stopping ISDN Voice Box Answering Machine: "
        "$IVAMD" -k && ( rm -f /var/run/ivamd.pid ; echo "ivamd" )
        ;;

    status)
        "$IVAMD" -c
        ;;

    reload)
        echo -n "Reloading ISDN Voice Box Answering Machine: "
        "$IVAMD" --reload && echo "ivamd"
        ;;

    force-reload|restart)
        "$0" stop
        sleep 3
        "$0" start
        ;;
    *)
        echo "Usage: $0 {start|stop|restart|reload|force-reload|status}"
        exit 1
esac

exit 0
